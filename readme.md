ColorMeRad
==========

Standardize the way you color your data and messages.


Installation
------------

Add this line to your application's Gemfile:

```ruby
gem 'color_me_rad'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install color_me_rad


Usage
-----

### Basic API

Print a hash that has keys and values colored according to configuration:

    > puts ColorMeRad.data({:foo => 'bar'}

Print a string that is colored using a custom setting
(must be set using one of the configuration methods below):

    > puts ColorMeRad.key('foo', :title)

Return the color for a given class:

    > ColorMeRad.color_for_class(String) #=> :black

Return the color for a given key:

    > ColorMeRad.color_for_key('String') #=> :black

`key` and `color_for_key` will look in both class settings
and custom settings for a color,
so give it any key that has a color associated with it
and it will give you the color back.


### Configuration

#### Enabling/Disabling

```
# via configuration
ColorMeRad.configure do |config|
  config.enabled = true  #=> turn on all coloring
  config.enabled = false #=> turn off all coloring
end


# via module api (useful for disabling during testing)

ColorMeRad.enabled = true  #=> turn on all coloring
ColorMeRad.enabled? #=> true

ColorMeRad.enabled = false #=> turn off all coloring
ColorMeRad.enabled? #=> false

```


#### Colors

If you intend to override color settings
or add your own, it is intended to happen via the configure block:


##### Override Existing Colors (class names)

A number of Ruby classes have colors set initially.
If you want to override these,
it can be done using `set_class_color` or `set_class_colors`.

Please note that if you attempt to set one of these keys
using the `set_color` or `set_colors` api,
an error will be raised.


```
ColorMeRad.configure do |config|
  config.set_class_color 'Integer', :green
  config.set_class_color 'String', :blue

  config.set_class_colors({
    'TrueClass'  => :light_magenta,
    'FalseClass' => :light_cyan,
  })
end
```


##### Add new color settings

Please note that if you attempt to set one of the class keys
using the `set_class_color` or `set_class_colors` api,
an error will be raised.


```
ColorMeRad.configure do |config|
  config.set_color :title, :green
  config.set_color :author, :blue

  config.set_colors({
    :error => :light_red,
    :warn  => :red,
  })
end
```


Development
-----------

After checking out the repo, run `bin/setup` to install dependencies.
Then, run `bundle exec rake spec` to run the tests.
You can also run `bin/console` for an interactive prompt
that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`.
To release a new version, update the version number in `version.rb`,
and then run `bundle exec rake release`,
which will create a git tag for the version,
push git commits and tags,
and push the `.gem` file to [rubygems.org](https://rubygems.org).


Contributing
------------

Bug reports and pull requests are welcome on Bitbucket
at https://bitbucket.org/toadjamb/gems_color_me_rad.
