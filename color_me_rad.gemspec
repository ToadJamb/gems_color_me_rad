Gem::Specification.new do |spec|
  spec.name          = 'color_me_rad'
  spec.version       = '0.0.4'
  spec.authors       = ['Travis Herrick']
  spec.email         = ['tthetoad@gmail.com']

  spec.summary       = 'Simple color console output'
  spec.description   = 'Color console output in a radically simple way'
  spec.homepage      = 'https://bitbucket.org/toadjamb/gems_color_me_rad'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      f.match(%r{^(test|spec|features)/})
    end
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rake_tasks'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'cane'
  spec.add_development_dependency 'gems'
end
