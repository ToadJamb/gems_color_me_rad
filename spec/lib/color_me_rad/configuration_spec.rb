require 'spec_helper'

require 'date'
require 'bigdecimal'

RSpec.describe ColorMeRad::Configuration do
  it_behaves_like 'in_out', :color_for_class, [String], :black
  it_behaves_like 'in_out', :color_for_class, [NilClass], :yellow

  it_behaves_like 'in_out', :color_for_key, [:foo], :light_yellow

  describe '#classes' do
    it 'contains valid colors' do
      subject.classes.values.each do |color|
        expect(ColorMeRad::Colorize::CODES.keys).to include color
      end
    end

    it 'contains valid class names' do
      subject.classes.keys.each do |class_name|
        next if class_name == 'Fixnum' # this generates a warning
        expect(Kernel.const_defined?(class_name)).to eq(true),
          "#{class_name} is not defined"
      end
    end
  end

  describe '#keys' do
    it 'contains valid colors' do
      subject.keys.values.each do |color|
        expect(ColorMeRad::Colorize::CODES.keys).to include color
      end
    end
  end

  describe '.set_color' do
    shared_examples 'set_color_success' do |args, expected|
      context "given #{args.inspect}" do
        let(:key) { args.first }

        before { subject.set_color(*args) }

        it "#keys returns #{expected}" do
          expect(subject.keys.slice(key)).to eq expected
        end
      end
    end

    it_behaves_like 'set_color_success', [:foo, :green], {:foo => :green}

    shared_examples 'set_color_error' do |args, expected|
      context "given #{args.inspect}" do
        it "#keys returns #{expected}" do
          expect{subject.set_color(*args)}.to raise_error(*expected)
        end
      end
    end

    it_behaves_like 'set_color_error', [:foo, :bar], [/bar.*not a valid color/]
    it_behaves_like 'set_color_error', ['String', :red], [/String.*is used/]
    it_behaves_like 'set_color_error', ['String', :foo], [/String.*is used/]
  end

  describe '.set_colors' do
    context 'given valid keys' do
      let(:keys) { {:foo => :red, :bar => :green} }

      before { subject.set_colors keys }

      it 'sets multiple keys' do
        expect(subject.color_for_key(:foo)).to eq :red
        expect(subject.color_for_key(:bar)).to eq :green
      end
    end

    context 'given invalid keys' do
      it 'raises an error' do
        expect{subject.set_colors('String' => :foo)}.to \
          raise_error(RuntimeError, /not a valid color/)
      end
    end

    context 'given something other than a hash' do
      it 'raises an error' do
        expect{subject.set_colors([:foo, :green])}.to \
          raise_error(RuntimeError, /not a hash/i)
      end
    end
  end

  describe '.set_class_color' do
    shared_examples 'class_key_success' do |args, expected|
      let(:key) { args.first }

      context "given #{args.inspect}" do
        before { subject.set_class_color(*args) }

        it "#keys returns #{expected}" do
          expect(subject.classes.slice(key)).to eq expected
        end
      end
    end

    it_behaves_like 'class_key_success', ['String', :red], {'String' => :red}
    it_behaves_like 'class_key_success', ['String', :blue], {'String' => :blue}

    shared_examples 'class_error' do |args, expected|
      context "given #{args.inspect}" do
        it "#keys returns #{expected}" do
          expect{subject.set_class_color(*args)}.to raise_error(*expected)
        end
      end
    end

    it_behaves_like 'class_error', ['String', :foo], [/foo.*not a valid color/]
    it_behaves_like 'class_error', [:foo, :red], [/key is not expected.*:foo/]
    it_behaves_like 'class_error', [:foo, :bar], [/is not expected/]
  end

  describe '.set_class_colors' do
    context 'given valid keys' do
      let(:keys) { {'String' => :red, 'Symbol' => :green} }

      before { subject.set_class_colors keys }

      it 'sets multiple keys' do
        expect(subject.color_for_key('String')).to eq :red
        expect(subject.color_for_key('Symbol')).to eq :green
      end
    end

    context 'given invalid keys' do
      it 'raises an error' do
        expect{subject.set_class_colors(:foo => :bar)}.to \
          raise_error(RuntimeError, /not a valid color/)
      end
    end

    context 'given something other than a hash' do
      it 'raises an error' do
        expect{subject.set_class_colors([:foo, :green])}.to \
          raise_error(RuntimeError, /not a hash/i)
      end
    end
  end
end
