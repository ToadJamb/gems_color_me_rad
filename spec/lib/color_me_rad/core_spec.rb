require 'spec_helper'

RSpec.describe ColorMeRad do
  let(:string)         { 'foo' }
  let(:color_args)     {[string, :green]}
  let(:string_colored) { "\e[32mfoo\e[0m" }

  describe '.configure' do
    it 'allows configuration properties to be set' do
      described_class.configure do |config|
        expect(config).to be_a ColorMeRad::Configuration
      end
    end
  end

  describe '.enabled=' do
    after { described_class.enable! }

    shared_examples 'enabled=' do |enabled, args, expected|
      context "given #{enabled.inspect}" do
        context "given colorize receives #{args.inspect}" do
          it "returns #{expected.inspect}" do
            described_class.enabled = enabled
            expect(described_class.colorize(*args)).to eq expected
          end
        end
      end
    end

    it_behaves_like 'enabled=', true, ['foo', :green], "\e[32mfoo\e[0m"
    it_behaves_like 'enabled=', false, ['foo', :green], 'foo'
  end

  describe '.enable!' do
    after { described_class.enable! }

    context 'given color is enabled' do
      before { described_class.enabled = true }

      it '.colorize returns strings with color' do
        described_class.enable!
        expect(described_class.colorize(*color_args)).to eq string_colored
      end
    end

    context 'given color is not enabled' do
      before { described_class.enabled = false }

      it '.colorize returns strings with color' do
        described_class.enable!
        expect(described_class.colorize(*color_args)).to eq string_colored
      end
    end
  end

  describe '.disable!' do
    after { described_class.enable! }

    context 'given color is enabled' do
      before { described_class.enabled = true }

      it '.colorize returns strings with color' do
        described_class.disable!
        expect(described_class.colorize(*color_args)).to eq string
      end
    end

    context 'given color is not enabled' do
      before { described_class.enabled = false }

      it '.colorize returns strings with color' do
        described_class.disable!
        expect(described_class.colorize(*color_args)).to eq string
      end
    end
  end

  describe '.colorize' do
    context 'given nested coloring' do
      it 'wraps the strings as expected' do
        msg = described_class.colorize('foo', :green, :white)
        msg = described_class.colorize("fizz#{msg}buzz",
                                       :light_red, :light_yellow)

        expect(msg).to eq \
          "\e[103m\e[91mfizz\e[47m\e[32mfoo\e[0m\e[103m\e[91mbuzz\e[0m"
      end
    end
  end

  describe '.data' do
    it 'works' do
      described_class.data 7
      described_class.data 'hello'
      described_class.data :foo
      described_class.data({:foo => ['bar', {:fizz => 'buzz'}]})
    end
  end

  describe '.key' do
    it 'works' do
      described_class.key 7, :foo
    end
  end

  #it 'works' do
  #  require 'date'
  #  [
  #    Fixnum,
  #    Integer,
  #    String,
  #    Date,
  #    DateTime,
  #    Time,
  #    #Decimal,
  #    TrueClass,
  #    FalseClass,
  #    NilClass,
  #    Symbol,
  #    Hash,
  #    Array,
  #    Float,
  #    Class,
  #  ].each do |klass|
  #    puts [klass, described_class.color_for_class(klass)].inspect
  #  end
  #end
end
