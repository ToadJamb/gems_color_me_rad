RSpec.shared_examples 'in_out' do |method, args, expected|
  describe ".#{method}" do
    context "given #{args.inspect}" do
      it "returns #{expected.inspect}" do
        expect(subject.send(method, *args)).to eq expected
      end
    end
  end
end
