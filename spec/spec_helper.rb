$VERBOSE = true

require 'color_me_rad'

base = File.expand_path(File.dirname(__FILE__))
path = File.join(base, 'support')
Dir["#{path}/**/*.rb"].each do |file|
  require file
end

RSpec.configure do |config|
  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.order = :random
  Kernel.srand config.seed
end
