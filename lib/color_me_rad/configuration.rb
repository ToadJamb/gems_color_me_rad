module ColorMeRad
  class Configuration
    attr_reader :classes, :keys

    class << self
      def current_config
        @configuration ||= new
      end
    end

    def initialize
      self.enabled       = true
      self.ivars         = false
      self.active_record = false

      @keys = {
        :constant => :yellow,
        :ivar     => :light_green,
        :syntax   => :light_black,
        :unknown  => :light_yellow,
      }

      @classes = {
        'Fixnum'        => :red,
        "#{Integer}"    => :red,
        "#{String}"     => :black,
        'Date'          => :light_magenta,
        'DateTime'      => :light_cyan,
        "#{Time}"       => :light_cyan,
        'BigDecimal'    => :red,
        "#{TrueClass}"  => :cyan,
        "#{FalseClass}" => :cyan,
        "#{NilClass}"   => :yellow,
        "#{Symbol}"     => :light_green,
        "#{Hash}"       => :yellow,
        "#{Array}"      => :yellow,
        "#{Float}"      => :red,
        "#{Class}"      => :yellow,
        "#{Module}"     => :yellow,
        "#{Range}"      => :yellow,
      }
    end

    def enabled=(value)
      @enabled = !!value
    end

    def enabled?
      @enabled
    end

    def ivars=(value)
      @ivars = !!value
    end

    def ivars?
      @ivars
    end

    def active_record=(value)
      @active_record = !!value
    end

    def active_record?
      @active_record
    end

    def color_for_class(klass)
      color_for_key klass.to_s
    end

    def color_for_key(key)
      color = @classes[key] || @keys[key]
      color ||= key if Colorize::CODES[key]
      color || @keys[:unknown]
    end

    def set_colors(keys)
      validate_hash keys
      keys.each do |key, color|
        set_color key, color
      end
    end

    def set_color(key, color)
      validate_key key, color
      @keys.merge! key => color
    end

    def set_class_colors(keys)
      validate_hash keys
      keys.each do |key, color|
        set_class_color key, color
      end
    end

    def set_class_color(key, color)
      validate_class_key key, color
      @classes.merge! key => color
    end

    private

    def validate_hash(value)
      return if value.is_a?(Hash)
      errors = []
      errors << "Not a hash: #{value.inspect}"
      show_errors errors
    end

    def validate_key(key, color)
      errors = []

      validate_color color, errors
      validate_unique_key key, errors

      show_errors errors
    end

    def validate_class_key(key, color)
      errors = []

      validate_color color, errors
      validate_used_key key, errors

      show_errors errors
    end

    def validate_color(color, errors)
      return if ColorMeRad::Colorize::CODES.keys.include?(color)
      errors << "#{color} is not a valid color"
    end

    def validate_unique_key(key, errors)
      return unless @classes[key]
      errors << '%s is used in class settings: use #set_class_color instead' % [
        key.inspect,
      ]
    end

    def validate_used_key(key, errors)
      return if @classes.keys.include?(key)
      errors << "key is not expected in class settings: #{key.inspect}"
    end

    def show_errors(errors)
      return if errors.empty?
      raise "#{ColorMeRad::Configuration}: " + errors.join('. ') + '.'
    end
  end
end
