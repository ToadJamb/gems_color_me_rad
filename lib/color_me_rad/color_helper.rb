module ColorMeRad
  module ColorHelper
    extend self

    def data(data)
      return hash(data) if data.is_a?(Hash)
      return array(data) if enumerable?(data)
      return ar_data(data) if active_record?(data)
      return ruby_ivars(data) if ivars?(data)

      colorize_data data
    end

    def key(data, key)
      color = ColorMeRad.color_for_key(key)
      add_color data, color
    end

    private

    def hash(hash)
      string = key('{', 'Hash')
      string += map_hash_data(hash)
      string + key('}', 'Hash')
    end

    def array(array)
      string = key('[', 'Array')

      string += array.each.map do |value|
        data value
      end.join(key(',', :syntax))

      string + key(']', 'Array')
    end

    def ruby_ivars(data)
      method = data.is_a?(Module) ? :as_module : :as_object

      send(method, data) do
        map_ivars data
      end
    end

    def ar_data(data)
      as_object(data) do
        map_ar_attributes data.attributes
      end
    end

    def as_object(data, &block)
      # this is the method ruby uses to 'encode' the object id shown by #to_s
      # shift the bit (multiply by 2) and use the hex representation
      id = (data.object_id << 1).to_s(16)

      '%s%s%s%s %s%s' % [
        syntax_constructs[:leader],
        key(data.class.to_s, :constant),
        syntax_constructs[:colon],
        key("0x#{id}", 'Integer'),
        yield,
        syntax_constructs[:closer],
      ]
    end

    def as_module(data, &block)
      '%s%s %s%s' % [
        syntax_constructs[:leader],
        key(data.to_s, :constant),
        yield,
        syntax_constructs[:closer],
      ]
    end

    def syntax_constructs
      @syntax_constructs ||= {
        :leader => key('#<', :syntax),
        :closer => key('>', :syntax),
        :colon  => key(':', :syntax),
      }
    end

    def colorize_data(data)
      color ||= color_for_data_class(data)
      data  = data.inspect if [String].include?(data.class)

      add_color data, color
    end

    def add_color(data, color)
      data = data.inspect if [NilClass, Symbol].include?(data.class)
      data = data.to_s unless data.is_a?(String)

      ColorMeRad.colorize data, color
    end

    def colorize(text, foreground = nil, background = nil)
      ColorMeRad.colorize text, foreground, background
    end

    def map_hash_data(hash)
      hash.each.map do |key, value|
        '%s%s%s' % [
          data(key),
          key('=>', :syntax),
          data(value),
        ]
      end.join(key(',', :syntax))
    end

    def map_ivars(data)
      data.instance_variables.map do |var|
        value = data.instance_variable_get(var)
        format_ivar var, value
      end.join(' ')
    end

    def format_ivar(var, value)
      '%s%s%s' % [
        key(var.to_s, :ivar),
        key('=', :syntax),
        data(value),
      ]
    end

    def map_ar_attributes(attrs)
      attrs.map do |key, value|
        '%s%s%s' % [
          key("@#{key}", :ivar),
          key('=', :syntax),
          data(value),
        ]
      end.join(' ')
    end

    def color_for_data_class(data)
      ColorMeRad.color_for_class data.class
    end

    def ivars?(data)
      return false unless ColorMeRad.ivars?
      return false unless data.respond_to?(:instance_variables)
      if defined?(ActiveRecord) && data.is_a?(ActiveRecord::Base)
        return false
      end
      return false if data.instance_variables.empty?
      true
    end

    def active_record?(data)
      return false unless ColorMeRad.active_record?
      return false unless defined?(ActiveRecord)
      data.is_a? ActiveRecord::Base
    end

    def enumerable?(data)
      return false if data.is_a?(Range)
      return false unless data.respond_to?(:each)
      return false if data.respond_to?(:keys)
      true
    end
  end
end
