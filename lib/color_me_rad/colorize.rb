module ColorMeRad
  module Colorize
    extend self

    CODES = {
      :black   => 0, :light_black   => 60,
      :red     => 1, :light_red     => 61,
      :green   => 2, :light_green   => 62,
      :yellow  => 3, :light_yellow  => 63,
      :blue    => 4, :light_blue    => 64,
      :magenta => 5, :light_magenta => 65,
      :cyan    => 6, :light_cyan    => 66,
      :white   => 7, :light_white   => 67,
      :default => 9
    }

    def colorize(text, foreground, background)
      return text unless ColorMeRad.enabled?

      text = set_foreground(text, foreground) if foreground
      text = set_background(text, background) if background

      text
    end

    def show_samples
      CODES.keys.permutation(2).each do |background, foreground|
        message = sample_message(background, foreground, 13)
        puts colorize(message, foreground, background)
      end
    end

    def colored?(text)
      return false unless text.is_a?(String)
      !!text.match(/\033\[\d{2,3}m/)
    end

    private

    def set_foreground(text, color)
      return text unless CODES[color]

      code = CODES[color] + 30
      text = text.split("\e[0m").map do |string|
        "\033[#{code}m#{string}\e[0m"
      end.join
    end

    def set_background(text, color)
      return text unless CODES[color]

      code = CODES[color] + 40
      text = text.split("\e[0m").map do |string|
        "\033[#{code}m#{string}\e[0m"
      end.join
    end

    def sample_message(background, foreground, pad)
      '  example of %s on %s  ' % [
        foreground.to_s.ljust(pad),
        background.to_s.ljust(pad),
      ]
    end
  end
end
