module ColorMeRad
  module Core
    # configuration methods

    def configure(&block)
      yield Configuration.current_config
    end

    def enabled=(value)
      configuration.enabled = value
    end

    def enable!
      configuration.enabled = true
    end

    def disable!
      configuration.enabled = false
    end

    def enabled?
      configuration.enabled?
    end

    def ivars?
      configuration.ivars?
    end

    def active_record?
      configuration.active_record?
    end


    # examples and samples

    def show_color_samples
      Colorize.show_samples
    end


    # colorization methods

    # TODO: test (officially)
    def colorize(data, foreground = nil, background = nil)
      Colorize.colorize data, foreground, background
    end

    # TODO: test (officially)
    def data(data)
      ColorHelper.data data
    end

    # TODO: test (officially)
    def key(data, key)
      ColorHelper.key data, key
    end

    # TODO: test
    def color_for_key(key)
      configuration.color_for_key key
    end

    # TODO: test
    def color_for_class(klass)
      configuration.color_for_class klass
    end

    # support

    def colored?(text)
      Colorize.colored? text
    end

    private

    def configuration
      Configuration.current_config
    end
  end

  extend Core
end
